package co.edu.uniquindio.android.electiva.cuestionarioacreditacion.actividades;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.ProgressBar;

import java.util.ArrayList;

import co.edu.uniquindio.android.electiva.cuestionarioacreditacion.R;
import co.edu.uniquindio.android.electiva.cuestionarioacreditacion.fragmentos.CreditosFragment;
import co.edu.uniquindio.android.electiva.cuestionarioacreditacion.util.AdministradorBD;
import co.edu.uniquindio.android.electiva.cuestionarioacreditacion.util.CRUD;
import co.edu.uniquindio.android.electiva.cuestionarioacreditacion.util.Utilidades;
import co.edu.uniquindio.android.electiva.cuestionarioacreditacion.vo.Pregunta;
import io.codetail.animation.SupportAnimator;

public class VistaPrincipal extends AppCompatActivity {

    private ArrayList<Pregunta> preguntas;
    private MediaPlayer mp;
    private CreditosFragment creditosFragment;
    private AdministradorBD adminBD;
    private Button botonIniciar;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_vista_principal);

        botonIniciar = (Button) findViewById(R.id.iniciar);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);


        mp = MediaPlayer.create(this, R.raw.intro);
        mp.start();

        adminBD = new AdministradorBD(this, 1);

        if (getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        preguntas = adminBD.obtenerInformacionBD();

        Utilidades.mostrarMensajePorConsola(preguntas.size()+"");

        if (preguntas.size() == 0) {
            if (!isOnline()) {
                mostrarDialogoConexion("No hay conexión a internet, por favor verifique su conexión y reinicie la aplicación", false);
            } else {
                cargarInformaciónDesdeElServicio();
            }
        }
        else{
            progressBar.setVisibility(View.INVISIBLE);
            botonIniciar.setVisibility(View.VISIBLE);
        }


        AdministradorBD.crearTabla();
        creditosFragment = new CreditosFragment();

    }

    private void cargarInformaciónDesdeElServicio() {
        HiloSecundario hilo = new HiloSecundario(getBaseContext());
        hilo.execute(1);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mp.pause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_acerca_de) {

            creditosFragment.setStyle(creditosFragment.STYLE_NO_TITLE, R.style.MiDialogo);
            creditosFragment.show(getSupportFragmentManager(), CreditosFragment.class.getSimpleName());

        }
        else if(item.getItemId() == R.id.menu_actualizar && preguntas.size() > 0){

            if (!isOnline()) {
                mostrarDialogoConexion("No hay conexión a internet, intente actualizar más tarde.", true);
            } else {
                adminBD.eliminarTodo();
                cargarInformaciónDesdeElServicio();
            }

        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mp.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mp.release();
        mp = null;
    }

    public void mostrarDialogoConexion(String mensaje, boolean hayDatos) {

        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
        dialogo1.setTitle("Importante");
        dialogo1.setMessage(mensaje);
        dialogo1.setCancelable(false);

        if( !hayDatos ){
            dialogo1.setPositiveButton("Cerrar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    finish();
                }
            });
        }
        else{
            dialogo1.setPositiveButton("Cerrar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                }
            });
        }

        dialogo1.show();

    }

    public void iniciarPartida(View view) {

        if (preguntas != null) {
            Intent intent = new Intent(this, VistaPreguntas.class);
            intent.putParcelableArrayListExtra(Utilidades.PREGUNTAS, preguntas);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if(preguntas.size() == 0){
            mostrarDialogoConexion("Se ha perdido la conexión a internet por favor " +
                    "verifique su conexión e inténtelo de nuevo", false);
        }

    }

    public void mostrarVista(View vista) {

        try {

            int cx = (vista.getLeft() + vista.getRight()) / 2;
            int cy = (vista.getTop() + vista.getBottom()) / 2;
            int radioFinal = Math.max(vista.getWidth(), vista.getHeight());

            SupportAnimator anim = io.codetail.animation.ViewAnimationUtils.createCircularReveal(vista, cx, cy, 0, radioFinal);
            vista.setVisibility(View.VISIBLE);

            anim.setInterpolator(new AccelerateDecelerateInterpolator());
            anim.setDuration(1000);
            anim.start();

        } catch (IllegalStateException e) {
            Log.v("Error", " error al cargar la animación " + e.getMessage());
        }

    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public void setPreguntas(ArrayList<Pregunta> preguntas) {
        this.preguntas = preguntas;
    }

    /**
     * Hilo encargado de manejar el proceso de obtencion de la información
     */
    public class HiloSecundario extends AsyncTask<Integer, Integer, Integer> {


        public Context getContext() {
            return context;
        }

        public void setContext(Context context) {
            this.context = context;
        }

        private Context context;
        private Pregunta pregunta;

        public HiloSecundario(Context context) {
            this.context = context;
            pregunta = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            botonIniciar.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }


        /**
         * Inicia apenas comienza el hilo
         *
         * @param params
         * @return
         */
        @Override
        protected Integer doInBackground(Integer... params) {

            if (params[0] == Utilidades.LISTAR_PREGUNTAS) {
                setPreguntas(CRUD.getListaDePregunta());
            } else if (params[0] == Utilidades.CARGAR_EN_BD) {

                for (Pregunta p : preguntas) {
                    adminBD.insertarPelicula(p.getEnunciado(), p.getOpcion1(), p.getOpcion2(), p.getOpcion3(), p.getOpcion4(), p.getRespuesta());
                }
            }

            return params[0];
        }

        /**
         * CARGA LAS PELICULAS QUE SE OBTUVIERON DE LAS PELICUALAS
         * @param integer
         */
        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            if (integer == Utilidades.LISTAR_PREGUNTAS) {
                HiloSecundario hiloSecundario = new HiloSecundario(VistaPrincipal.this);
                hiloSecundario.execute(2);
            } else if (integer == Utilidades.CARGAR_EN_BD) {
                progressBar.setVisibility(View.INVISIBLE);
                mostrarVista(botonIniciar);
            }

        }
    }

}
