package co.edu.uniquindio.android.electiva.cuestionarioacreditacion.util;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import co.edu.uniquindio.android.electiva.cuestionarioacreditacion.vo.Pregunta;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.util.EntityUtils;

/**
 * Created by Jhon Jaime on 24/05/2016.
 */
public class CRUD {



    /**
     * Se encarga de consumir el listado de peliculas desde el servicio
     * @return las peliculas alojadas en el servicio
     */
    public static ArrayList<Pregunta> getListaDePregunta(){

        ArrayList<Pregunta> preguntas = new ArrayList<>();
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(Utilidades.URL_SERVICIO);
        request.setHeader("content-type", "application/json");

        try {
            HttpResponse resp = httpClient.execute(request);
            String respStr = EntityUtils.toString(resp.getEntity());

            Gson gson = new Gson();
            Type tipoListaFilms = new TypeToken<ArrayList<Pregunta>>(){}.getType();

            preguntas = gson.fromJson(respStr, tipoListaFilms);

        } catch (Exception e) {
            Log.v(CRUD.class.getSimpleName(), e.getMessage());
            return null;
        }
        return preguntas;
    }


}
