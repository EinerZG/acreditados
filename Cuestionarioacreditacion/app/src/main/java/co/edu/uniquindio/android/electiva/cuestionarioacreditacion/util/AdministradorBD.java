package co.edu.uniquindio.android.electiva.cuestionarioacreditacion.util;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import co.edu.uniquindio.android.electiva.cuestionarioacreditacion.vo.Pregunta;

/**
 * Created by EinerZG on 26/07/16.
 */
public class AdministradorBD {

    private AcreditadosSQLiteHelper usdbh;
    private static SQLiteDatabase db;

    public AdministradorBD(Context context, int version) {
        usdbh = new AcreditadosSQLiteHelper(context,
                Utilidades.NOMBRE_BD , null, version);
        db = usdbh.getWritableDatabase();
    }

    /**
     * permite crear una tabla para alamcenar todas las preguntas en la base de datos
     * @return consulta para crear la tabla
     */
    public static String crearTabla(){

        String crearTabla = "CREATE TABLE $ ( $ TEXT, $ TEXT, $ TEXT, $ TEXT, $ TEX, $ TEX)";
        StringBuilder builder = new StringBuilder(crearTabla);

        builder.replace(builder.indexOf("$"),
                crearTabla.indexOf("$")+1,Utilidades.NOMBRE_TABLA); builder.replace(builder.indexOf("$"),
                builder.indexOf("$")+1,Utilidades.CAMPOS_TABLA[0]);
        builder.replace(builder.indexOf("$"), builder.indexOf("$")+1,Utilidades.CAMPOS_TABLA[1]);
        builder.replace(builder.indexOf("$"), builder.indexOf("$")+1,Utilidades.CAMPOS_TABLA[2]);
        builder.replace(builder.indexOf("$"), builder.indexOf("$")+1,Utilidades.CAMPOS_TABLA[3]);
        builder.replace(builder.indexOf("$"), builder.indexOf("$")+1,Utilidades.CAMPOS_TABLA[4]);
        builder.replace(builder.indexOf("$"), builder.indexOf("$")+1,Utilidades.CAMPOS_TABLA[5]);

        return builder.toString();
    }

    /**
     * Se encarga de cargar toda la información desde la base datos
     * @return una lista preguntas
     */
    public static ArrayList<Pregunta> obtenerInformacionBD() {

        ArrayList<Pregunta> preguntas = new ArrayList<>();
        Cursor c = db.query(Utilidades.NOMBRE_TABLA, Utilidades.CAMPOS_TABLA, null, null, null, null, null);

        if (c.moveToFirst()) {
            do {

                String enunciado = c.getString(0);
                String opcion1 = c.getString(1);
                String opcion2 = c.getString(2);
                String opcion3 = c.getString(3);
                String opcion4 = c.getString(4);
                String correcta = c.getString(5);

                preguntas.add(new Pregunta(enunciado, opcion1, opcion2, opcion3, opcion4, correcta));

            } while (c.moveToNext());
        }

        return preguntas;
    }

    /**
     * Permite insertar la información de las preguntas en la base de datos
     * @param campos información que se desea agregar
     */
    public void insertarPelicula(String... campos) {

        String insertar = "INSERT INTO $ ($,$,$,$,$,$) VALUES ( '$', '$', '$', '$', '$', '$')";

        StringBuilder builder = new StringBuilder(insertar);

        builder.replace(builder.indexOf("$"), builder.indexOf("$") + 1, Utilidades.NOMBRE_TABLA);

        builder.replace(builder.indexOf("$"), builder.indexOf("$") + 1, Utilidades.CAMPOS_TABLA[0]);
        builder.replace(builder.indexOf("$"), builder.indexOf("$") + 1, Utilidades.CAMPOS_TABLA[1]);
        builder.replace(builder.indexOf("$"), builder.indexOf("$") + 1, Utilidades.CAMPOS_TABLA[2]);
        builder.replace(builder.indexOf("$"), builder.indexOf("$") + 1, Utilidades.CAMPOS_TABLA[3]);
        builder.replace(builder.indexOf("$"), builder.indexOf("$") + 1, Utilidades.CAMPOS_TABLA[4]);
        builder.replace(builder.indexOf("$"), builder.indexOf("$") + 1, Utilidades.CAMPOS_TABLA[5]);


        builder.replace(builder.indexOf("$"), builder.indexOf("$") + 1,
                campos[0]);
        builder.replace(builder.indexOf("$"), builder.indexOf("$") + 1,
                campos[1]);
        builder.replace(builder.indexOf("$"), builder.indexOf("$") + 1,
                campos[2]);
        builder.replace(builder.indexOf("$"), builder.indexOf("$") + 1,
                campos[3]);
        builder.replace(builder.indexOf("$"), builder.indexOf("$") + 1,
                campos[4]);
        builder.replace(builder.indexOf("$"), builder.indexOf("$") + 1,
                campos[5]);

        ejecutarConsulta(builder.toString());
    }

    /**
     * elimina toda la información almacenada en la tabla de preguntas
     */
    public static void eliminarTodo(){
        ejecutarConsulta("delete from "+ Utilidades.NOMBRE_TABLA);
    }

    /**
     * permite ejecutar la consulta deseada
     */
    public static void ejecutarConsulta(String consulta){
        db.execSQL(consulta);
    }


}
