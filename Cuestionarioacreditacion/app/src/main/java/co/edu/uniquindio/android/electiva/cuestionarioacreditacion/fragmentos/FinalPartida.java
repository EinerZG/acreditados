package co.edu.uniquindio.android.electiva.cuestionarioacreditacion.fragmentos;

import android.app.DialogFragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import co.edu.uniquindio.android.electiva.cuestionarioacreditacion.R;
import co.edu.uniquindio.android.electiva.cuestionarioacreditacion.util.Utilidades;


public class FinalPartida extends DialogFragment {

    private Integer puntaje;
    private String tiempo;
    private boolean gano;

    public FinalPartida() {
        // Required empty public constructor
    }

    public FinalPartida(Integer puntaje, String tiempo, boolean gano) {
        this.puntaje = puntaje;
        this.tiempo = tiempo;
        this.gano = gano;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setCancelable(false);
        View v= inflater.inflate(R.layout.fragment_final_partida, container, false);

        if( gano ){
            int color= ContextCompat.getColor(v.getContext(), R.color.colorPrimary);
            v.setBackgroundColor(color);
        }

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(puntaje == null) {
            onRestored(savedInstanceState);
        }
        TextView txtPuntaje = (TextView)getView().findViewById(R.id.puntaje_total);
        txtPuntaje.setText(puntaje.toString());

        TextView txtTiempo = (TextView)getView().findViewById(R.id.tiempo_total);
        txtTiempo.setText(tiempo);



    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(Utilidades.PUNTAJE, puntaje);
        outState.putString(Utilidades.TIEMPO, tiempo);
    }

    public void onRestored(Bundle savedInstanceState) {
        puntaje = savedInstanceState.getInt(Utilidades.PUNTAJE);
        tiempo = savedInstanceState.getString(Utilidades.TIEMPO);
    }
}
