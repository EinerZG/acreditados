package co.edu.uniquindio.android.electiva.cuestionarioacreditacion.vo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Clase Pregunta
 *
 * @Autor jhon jaime ramirez
 */
public class Pregunta implements Parcelable {

    /**
     * variable que representa el titulo de la pregunta
     */
    private String enunciado;


    private String opcion1;

    private String opcion2;

    private String opcion3;

    private String opcion4;

    private String correcta;

    /**
     * Constructor de la clase pregunta
     *
     * @param enunciado de la pregunta
     * @param opcion1
     * @param opcion2
     * @param opcion3
     * @param opcion4
     * @param correcta
     */
    public Pregunta(String enunciado, String opcion1, String opcion2, String opcion3, String opcion4, String correcta) {
        this.enunciado = enunciado;
        this.opcion1 = opcion1;
        this.opcion2 = opcion2;
        this.opcion3 = opcion3;
        this.opcion4 = opcion4;
        this.correcta = correcta;
    }

    protected Pregunta(Parcel in) {
        enunciado = in.readString();
        opcion1 = in.readString();
        opcion2 = in.readString();
        opcion3 = in.readString();
        opcion4 = in.readString();
        correcta = in.readString();
    }

    public static final Creator<Pregunta> CREATOR = new Creator<Pregunta>() {
        @Override
        public Pregunta createFromParcel(Parcel in) {
            return new Pregunta(in);
        }

        @Override
        public Pregunta[] newArray(int size) {
            return new Pregunta[size];
        }
    };

    /**
     * @return
     */
    public String getEnunciado() {
        return enunciado;
    }

    /**
     * Modifica el titulo de la pregunta
     *
     * @param enunciado variable a cambiar
     */
    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    /**
     * devuelve la opcion 1
     *
     * @return opcion1;
     */
    public String getOpcion1() {
        return opcion1;
    }

    public void setOpcion1(String opcion1) {
        this.opcion1 = opcion1;
    }

    public String getOpcion2() {
        return opcion2;
    }

    public void setOpcion2(String opcion2) {
        this.opcion2 = opcion2;
    }

    public String getOpcion3() {
        return opcion3;
    }

    public void setOpcion3(String opcion3) {
        this.opcion3 = opcion3;
    }

    public String getOpcion4() {
        return opcion4;
    }

    public void setOpcion4(String opcion4) {
        this.opcion4 = opcion4;
    }

    public String getRespuesta() {
        return correcta;
    }

    public void setRespuesta(String respuesta) {
        this.correcta = respuesta;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(enunciado);
        dest.writeString(opcion1);
        dest.writeString(opcion2);
        dest.writeString(opcion3);
        dest.writeString(opcion4);
        dest.writeString(correcta);
    }
}
