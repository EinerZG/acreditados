package co.edu.uniquindio.android.electiva.cuestionarioacreditacion.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by EinerZG on 26/07/16.
 * Esta clase se encarga de manejar la base de datos de la aplicación
 */
public class AcreditadosSQLiteHelper extends SQLiteOpenHelper{

    public AcreditadosSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(AdministradorBD.crearTabla());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
