package co.edu.uniquindio.android.electiva.cuestionarioacreditacion.fragmentos;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import co.edu.uniquindio.android.electiva.cuestionarioacreditacion.R;

/**
 * Este fragmento se encarga de mostrar a detalle los creditos de la aplicación
 */
public class CreditosFragment extends DialogFragment {

    public CreditosFragment() {}

    /**
     * Inicialización de vista y sus elementos
     * @param inflater permite inflar el view
     * @param container contenedor del view
     * @param savedInstanceState instancia para salvar información
     * @return la vista que ha sido creada
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_creditos, container, false);

        ImageButton btnCerrar = (ImageButton)(v.findViewById(R.id.creditos_btn_cerrar));
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return v;
    }

}
