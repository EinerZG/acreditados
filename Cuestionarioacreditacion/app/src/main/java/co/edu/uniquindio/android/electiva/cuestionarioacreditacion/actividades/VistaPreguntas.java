package co.edu.uniquindio.android.electiva.cuestionarioacreditacion.actividades;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

import co.edu.uniquindio.android.electiva.cuestionarioacreditacion.R;
import co.edu.uniquindio.android.electiva.cuestionarioacreditacion.util.Utilidades;
import co.edu.uniquindio.android.electiva.cuestionarioacreditacion.vo.Pregunta;
import io.codetail.animation.SupportAnimator;

public class VistaPreguntas extends AppCompatActivity implements View.OnClickListener {

    public ArrayList<Pregunta> preguntasUtilizadas;
    public ArrayList<Pregunta> preguntas;
    public ContadorTiempo timer;


    private View[] vistasBien;
    private View[] vistasMal;
    private View[] respuestas;

    /**
     * Variable que representa la respuesta de la pregunta
     */
    private String respuesta;

    private Integer puntaje;
    private Integer tiempoTotal;
    private String tiempoF;
    private Boolean ayuda5050;
    private Boolean ayudaPasar;
    private boolean primerPlano;

    private Boolean jugo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        preguntasUtilizadas = getIntent().getParcelableArrayListExtra(Utilidades.PREGUNTAS_UTILIZADAS);
        preguntas = getIntent().getParcelableArrayListExtra(Utilidades.PREGUNTAS);
        puntaje = getIntent().getIntExtra(Utilidades.PUNTAJE, 0);
        tiempoTotal = getIntent().getIntExtra(Utilidades.TIEMPO, 0);
        ayuda5050 = getIntent().getBooleanExtra(Utilidades.AYUDA5050, true);
        ayudaPasar = getIntent().getBooleanExtra(Utilidades.AYUDAPASAR, true);
        jugo = true;
        primerPlano= true;

        if (preguntas == null) {
            onRestoreInstanceState(savedInstanceState);
        }

        MediaPlayer mp = MediaPlayer.create(this, R.raw.click);
        mp.start();

        setContentView(R.layout.activity_vista_preguntas);

        findViewById(R.id.card_respuesta).setOnClickListener(this);
        findViewById(R.id.card_respuesta2).setOnClickListener(this);
        findViewById(R.id.card_respuesta3).setOnClickListener(this);
        findViewById(R.id.card_respuesta4).setOnClickListener(this);

        init();

        if (preguntasUtilizadas == null)
            preguntasUtilizadas = new ArrayList<>();

        timer = new ContadorTiempo();
        timer.execute();



        cargarPreguntaAleatoria();

    }

    @Override
    protected void onStop() {
        super.onStop();
        primerPlano= false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        primerPlano= true;
    }

    public void cargarPreguntaAleatoria() {

        if (preguntas.size() > 0) {

            int preguntaAleatoria = (int) (Math.random() * preguntas.size());
            Pregunta pregunta = preguntas.get(preguntaAleatoria);
            preguntasUtilizadas.add(pregunta);
            preguntas.remove(preguntaAleatoria);

            preguntasUtilizadas.add(pregunta);
            ((TextView) findViewById(R.id.view_pregunta)).setText(pregunta.getEnunciado());
            ((TextView) findViewById(R.id.opcion1)).setText(pregunta.getOpcion1());
            ((TextView) findViewById(R.id.opcion2)).setText(pregunta.getOpcion2());
            ((TextView) findViewById(R.id.opcion3)).setText(pregunta.getOpcion3());
            ((TextView) findViewById(R.id.opcion4)).setText(pregunta.getOpcion4());
            respuesta = pregunta.getRespuesta();

            Log.v("Correcta", " es: " + respuesta);

        } else {

            segundosAMinutos();
            Utilidades.mostrarFragmentFinalPartida(getFragmentManager(), VistaPreguntas.class.getSimpleName(),
                    puntaje, tiempoF, true);

            Toast.makeText(this, "Felicidades ha ganado", Toast.LENGTH_LONG).show();
        }

    }

    public void init() {

        vistasBien = new View[4];
        vistasMal = new View[4];
        respuestas = new View[4];

        vistasBien[0] = findViewById(R.id.vista_bien);
        vistasBien[1] = findViewById(R.id.vista_bien2);
        vistasBien[2] = findViewById(R.id.vista_bien3);
        vistasBien[3] = findViewById(R.id.vista_bien4);

        vistasMal[0] = findViewById(R.id.vista_error);
        vistasMal[1] = findViewById(R.id.vista_error2);
        vistasMal[2] = findViewById(R.id.vista_error3);
        vistasMal[3] = findViewById(R.id.vista_error4);

        respuestas[0] = findViewById(R.id.card_respuesta);
        respuestas[1] = findViewById(R.id.card_respuesta2);
        respuestas[2] = findViewById(R.id.card_respuesta3);
        respuestas[3] = findViewById(R.id.card_respuesta4);

    }

    public void ocultarVista(final View vista) {

        if (vista.getVisibility() == View.VISIBLE) {

            int cx = (vista.getLeft() + vista.getRight()) / 2;
            int cy = (vista.getTop() + vista.getBottom()) / 2;

            int radioInicial = vista.getWidth();


            SupportAnimator anim = io.codetail.animation.ViewAnimationUtils.createCircularReveal(vista, cx, cy, radioInicial, 0);

            anim.addListener(new SupportAnimator.SimpleAnimatorListener() {

                @Override
                public void onAnimationEnd() {
                    super.onAnimationEnd();
                    vista.setVisibility(View.INVISIBLE);
                    //TODO aqui se mostraran las erespuestas bnas o malas en el contador
                }

            });

            anim.setInterpolator(new AccelerateDecelerateInterpolator());
            anim.setDuration(400);

            anim.start();
        }
    }

    /**
     * Permite ejecura el sonido segun el id
     * @param idSonido id del sonido
     */
    private void ejecutarSonido(int idSonido){

        try{
            (MediaPlayer.create(this, idSonido)).start();
        }catch (Exception e){
            Utilidades.mostrarMensajePorConsola("Error audio: "+e.getMessage());

        }

    }

    public void mostrarVista(View vista) {

        int cx = (vista.getLeft() + vista.getRight()) / 2;
        int cy = (vista.getTop() + vista.getBottom()) / 2;
        int radioFinal = Math.max(vista.getWidth(), vista.getHeight());

        SupportAnimator anim = io.codetail.animation.ViewAnimationUtils.createCircularReveal(vista, cx, cy, 0, radioFinal);
        vista.setVisibility(View.VISIBLE);

        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.setDuration(1000);
        anim.start();

    }

    public void mostrarVistaYFramento(final View vista) {
        int cx = (vista.getLeft() + vista.getRight()) / 2;
        int cy = (vista.getTop() + vista.getBottom()) / 2;
        int radioFinal = Math.max(vista.getWidth(), vista.getHeight());

        SupportAnimator anim = io.codetail.animation.ViewAnimationUtils.createCircularReveal(vista, cx, cy, 0, radioFinal);
        vista.setVisibility(View.VISIBLE);

        anim.addListener(new SupportAnimator.SimpleAnimatorListener() {

            @Override
            public void onAnimationEnd() {
                super.onAnimationEnd();
                segundosAMinutos();

                Utilidades.mostrarFragmentFinalPartida(getFragmentManager(), VistaPreguntas.class.getSimpleName(),
                        puntaje, tiempoF, preguntas.size() == 0);
            }

        });

        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.setDuration(400);
        anim.start();

        timer.cancel(true);
    }

    /**
     * Muestra la vista correcta sin desplegar el fragmento
     *
     * @param vista
     */
    public void mostrarVistaCorrecta(final View vista) {

        int cx = (vista.getLeft() + vista.getRight()) / 2;
        int cy = (vista.getTop() + vista.getBottom()) / 2;
        int radioFinal = Math.max(vista.getWidth(), vista.getHeight());

        SupportAnimator anim = io.codetail.animation.ViewAnimationUtils.createCircularReveal(vista, cx, cy, 0, radioFinal);
        vista.setVisibility(View.VISIBLE);

        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.setDuration(400);
        anim.start();

        anim.addListener(new SupportAnimator.SimpleAnimatorListener() {

            @Override
            public void onAnimationEnd() {
                super.onAnimationEnd();
                reiniciar();

            }

        });

    }

    public void mostrarImagenCorrecta() {

        ejecutarSonido(R.raw.correcto);

        final View imagen = findViewById(R.id.contador);

        if (imagen.getVisibility() == View.VISIBLE) {

            int cx = (imagen.getLeft() + imagen.getRight()) / 2;
            int cy = (imagen.getTop() + imagen.getBottom()) / 2;

            int radioInicial = imagen.getWidth();


            SupportAnimator anim = io.codetail.animation.ViewAnimationUtils.createCircularReveal(imagen, cx, cy, radioInicial, 0);

            anim.addListener(new SupportAnimator.SimpleAnimatorListener() {

                @Override
                public void onAnimationEnd() {
                    super.onAnimationEnd();
                    imagen.setVisibility(View.INVISIBLE);
                    //TODO aqui se debe pasar a la sgte pregutna
                    mostrarVistaCorrecta(findViewById(R.id.correcta));

                }

            });

            anim.setInterpolator(new AccelerateDecelerateInterpolator());
            anim.setDuration(400);

            anim.start();
        }

    }

    public void mostrarImagenIncorrecta() {

        ejecutarSonido(R.raw.incorrecto);

        final View imagen = findViewById(R.id.contador);

        if (imagen.getVisibility() == View.VISIBLE) {

            int cx = (imagen.getLeft() + imagen.getRight()) / 2;
            int cy = (imagen.getTop() + imagen.getBottom()) / 2;

            int radioInicial = imagen.getWidth();


            SupportAnimator anim = io.codetail.animation.ViewAnimationUtils.createCircularReveal(imagen, cx, cy, radioInicial, 0);

            anim.addListener(new SupportAnimator.SimpleAnimatorListener() {

                @Override
                public void onAnimationEnd() {
                    super.onAnimationEnd();
                    imagen.setVisibility(View.INVISIBLE);
                    mostrarVistaYFramento(findViewById(R.id.incorrecta));
                }

            });

            anim.setInterpolator(new AccelerateDecelerateInterpolator());
            anim.setDuration(1000);

            anim.start();
        }

    }

    public void reiniciar() {
        reiniciarActivity(this);
    }

    //reinicia una Activity
    public void reiniciarActivity(Activity actividad) {
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra(Utilidades.PREGUNTAS_UTILIZADAS, preguntasUtilizadas);
        intent.putParcelableArrayListExtra(Utilidades.PREGUNTAS, preguntas);
        intent.putExtra(Utilidades.PUNTAJE, puntaje);
        intent.putExtra(Utilidades.TIEMPO, tiempoTotal);
        intent.putExtra(Utilidades.AYUDA5050, ayuda5050);
        intent.putExtra(Utilidades.AYUDAPASAR, ayudaPasar);
        intent.setClass(actividad, actividad.getClass());
        //llamamos a la actividad
        actividad.startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        //finalizamos la actividad actual
        actividad.finish();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putParcelableArrayList(Utilidades.PREGUNTAS_UTILIZADAS, preguntasUtilizadas);
        outState.putParcelableArrayList(Utilidades.PREGUNTAS, preguntas);
        outState.putInt(Utilidades.PUNTAJE, puntaje);
        outState.putInt(Utilidades.TIEMPO, tiempoTotal);
        outState.putBoolean(Utilidades.AYUDA5050, ayuda5050);
        outState.putBoolean(Utilidades.AYUDAPASAR, ayudaPasar);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (preguntas == null) {

            preguntasUtilizadas = savedInstanceState.getParcelableArrayList(Utilidades.PREGUNTAS_UTILIZADAS);
            preguntas = savedInstanceState.getParcelableArrayList(Utilidades.PREGUNTAS);
            puntaje = savedInstanceState.getInt(Utilidades.PUNTAJE);
            tiempoTotal = savedInstanceState.getInt(Utilidades.TIEMPO);
            ayuda5050 = savedInstanceState.getBoolean(Utilidades.AYUDA5050);
            ayudaPasar = savedInstanceState.getBoolean(Utilidades.AYUDAPASAR);

        }
    }

    public void segundosAMinutos() {

        int min = tiempoTotal / 60;
        int seg = tiempoTotal - (min * 60);
        if (seg < 10) {
            tiempoF = "Tiempo: " + min + ":0" + seg;
        } else {
            tiempoF = "Tiempo: " + min + ":" + seg;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel(true);
    }

    @Override
    public void onClick(View v) {

        if (jugo) {
            String[] letras = {"A", "B", "C", "D"};

            int respCorecta = Integer.parseInt(respuesta.trim());

            for (int i = 0; i < respuestas.length; i++) {

                if (v == respuestas[i]) {

                    if (i == respCorecta - 1) {

                        mostrarVista(vistasBien[i]);
                        mostrarImagenCorrecta();
                        puntaje++;
                        jugo = false;
                        break;

                    } else {

                        mostrarVista(vistasBien[respCorecta - 1]);
                        mostrarVista(vistasMal[i]);
                        mostrarImagenIncorrecta();
                        jugo = false;
                        break;
                    }
                }
            }
        }
    }

    public void salir(View view) {
        ejecutarSonido(R.raw.click);

        onBackPressed();
    }

    public void ayudarMitad(View view) {

        ejecutarSonido(R.raw.click);

        if (ayuda5050) {

            Random rnd = new Random();
            String[] opciones = {"A", "B", "C", "D"};
            int contador = 0;
            int nums[] = new int[2];
            Integer aux = null;

            do {

                int numAleatorio = rnd.nextInt(opciones.length);

                if (!String.valueOf(numAleatorio+1).equals(respuesta)) {

                    nums[contador] = numAleatorio;
                    contador++;

                    if (aux == null) {
                        aux = numAleatorio;
                    } else {
                        if (numAleatorio == aux) {
                            contador--;
                        }
                    }
                }
            } while (contador < 2);

            mostrarVista(vistasMal[nums[0]]);
            mostrarVista(vistasMal[nums[1]]);
            ayuda5050 = false;
        } else {
            Snackbar snackbar = Snackbar.make(view, "Ya no cuenta con esta ayuda", Snackbar.LENGTH_SHORT);
            snackbar.getView().setBackgroundColor(Color.rgb(167, 9, 9));
            snackbar.show();
        }
    }

    public void pasarPregunta(View view) {
        if (ayudaPasar) {
            ayudaPasar = false;
            reiniciar();
        } else {

            ejecutarSonido(R.raw.click);

            Snackbar snackbar = Snackbar.make(view, "Ya no cuenta con esta ayuda", Snackbar.LENGTH_SHORT);
            snackbar.getView().setBackgroundColor(Color.rgb(167, 9, 9));
            snackbar.show();
        }
    }

    /**
     * Esta parte del código se encarga de hacer avanzar el tiempo
     */
    public class ContadorTiempo extends AsyncTask<Integer, Integer, Integer> {

        private int tiempo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            tiempo = 0;
        }

        @Override
        protected Integer doInBackground(Integer... params) {

            try {
                for (; tiempo <= 59; tiempo++) {
                    Thread.sleep(1000);

                    if( !primerPlano ){
                        tiempo--;
                        continue;
                    }

                    tiempoTotal++;
                }
            } catch (InterruptedException i) {
            }

            return null;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            ejecutarSonido(R.raw.alarma);
            mostrarVistaYFramento(findViewById(R.id.fin_tiempo));

        }
    }
}
