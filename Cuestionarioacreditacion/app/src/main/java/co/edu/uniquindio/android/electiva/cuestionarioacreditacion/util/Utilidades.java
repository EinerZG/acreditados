package co.edu.uniquindio.android.electiva.cuestionarioacreditacion.util;

import android.app.FragmentManager;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import co.edu.uniquindio.android.electiva.cuestionarioacreditacion.R;
import co.edu.uniquindio.android.electiva.cuestionarioacreditacion.fragmentos.FinalPartida;
import co.edu.uniquindio.android.electiva.cuestionarioacreditacion.vo.Pregunta;

/**
 * Created by Carlos on 26/5/2016.
 */
public class Utilidades {

    //public final static String URL_SERVICIO = "http://192.168.1.15:3000/api/preguntas";
    public final static String URL_SERVICIO = "http://acreditacionuq-jc27b.rhcloud.com/pregunta";
    public static final int LISTAR_PREGUNTAS = 1;
    public static final String PREGUNTAS = "preguntas";
    public static final String PREGUNTAS_UTILIZADAS = "preguntas_utilizadas";
    public static final String PUNTAJE = "puntaje";
    public static final String TIEMPO = "tiempo";
    public static final String AYUDA5050 = "ayuda5050";
    public static final String AYUDAPASAR = "ayudaParar";

    //Para la configuración de la base de datos
    public static final int CARGAR_EN_BD = 2;
    public static final String NOMBRE_BD = "AcreditadosBD";
    public static final String NOMBRE_TABLA = "Preguntas";
    public static final String CAMPOS_TABLA[] = new String[]{"enunciado","opcion1", "opcion2", "opcion3", "opcion4", "correcta"};

    /**
     * Se encarga de converir un String formato JSON a una Película
     * @param jsonPregunta string en formato JSON * @return pregunta resultante de la conversión
     */
    public static Pregunta convertirJSONAPregunta(String jsonPregunta) {
        Gson gson = new Gson();
        Pregunta pregunta = gson.fromJson(jsonPregunta, Pregunta.class);
        return pregunta;
    }

    /**
     * Se encarga de convertir una pregunta en un JSON
     * @param pregunta pregunta que se desea transformar
     * <p/>
     *
     * @return cadena en formato de json de pregunta
     */
    public static String convertirPreguntaAJSON(Pregunta pregunta) {
        Gson gson = new Gson();
        String json = gson.toJson(pregunta);
        return json;
    }

    public static void  mostrarMensaje(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void mostrarMensajePorConsola(String mensaje){
        Log.v(Utilidades.class.getSimpleName(),mensaje);
    }

    public static void mostrarFragmentFinalPartida(FragmentManager fr, String nameClass, Integer puntaje, String tiempo, boolean gano) {
        FinalPartida finPartida = new FinalPartida(puntaje, tiempo, gano);
        finPartida.setStyle(finPartida.STYLE_NO_TITLE, R.style.MiDialogo);
        finPartida.show(fr, nameClass);
    }

}
